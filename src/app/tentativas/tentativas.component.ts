import { Component, OnInit, OnChanges, Input } from '@angular/core';

import { Coracao } from '../shard/coracao.model';

@Component({
  selector: 'app-tentativas',
  templateUrl: './tentativas.component.html',
  styleUrls: ['./tentativas.component.css']
})
export class TentativasComponent implements OnInit, OnChanges {
  @Input() tentativas;

  public coracaos: Coracao[] = [
    new Coracao(true),
    new Coracao(true),
    new Coracao(true)
  ];

  constructor() {
    console.log(this.coracaos);

  }

  ngOnChanges() {
    // this.tentativas
    // this.coracao.length
    if (this.tentativas !== this.coracaos.length) {
      const indice = this.coracaos.length - this.tentativas;
      this.coracaos[indice - 1].cheio = false;
    }
    console.log('tentativas recebidas do painel: ', this.tentativas);
  }

  ngOnInit() {


  }

}
