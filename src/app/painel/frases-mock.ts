import { Frase } from '../shard/frase.model';

// poderia ser string, number e etc, o tipopo dessa variavel FRASES.
export const FRASES: Frase[] = [
    // Array de objetos
    { fraseEng: 'I like to learn', frasePtBr: 'Eu gosto de aprender' },
    { fraseEng: 'I watch tv ', frasePtBr: 'Eu assisto televisao' },
    { fraseEng: 'How are you?', frasePtBr: 'Como vai você?' },
    { fraseEng: 'I eat bread.', frasePtBr: 'Eu como pão' }
];
