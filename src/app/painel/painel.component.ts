/* tslint:disable:no-trailing-whitespace */
import { Component, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { Frase } from '../shard/frase.model';
import { FRASES } from './frases-mock';
@Component({
  selector: 'app-painel',
  templateUrl: './painel.component.html',
  styleUrls: ['./painel.component.css']
})

// OnInit é uma interface
export class PainelComponent implements OnInit, OnDestroy {

  public frases: Frase[] = FRASES;
  instrucao = 'Traduza a frase.';
  resposta = '';

  rodada = 0;
  rodadaFrase: Frase;

  progresso = 0;

  tentativas = 3;

  @Output() encerrarJogo: EventEmitter<string> = new EventEmitter();

  constructor() {
    this.atualizaRodada();
  }

  ngOnInit() {

  }

  ngOnDestroy() {
    console.log('Componente painel foi destruido');
  }

  atualizaResposta(resposta: Event): void {

    this.resposta = ((resposta.target as HTMLInputElement).value);
    // console.log(this.resposta);
  }

  verificarResposta(): void {
    if (this.rodadaFrase.frasePtBr === this.resposta) {
      alert('A tradução esta correta ');
      // Trocar pergunta da rodada
      this.rodada = this.rodada + 1;

      this.progresso = this.progresso + (100 / this.frases.length);

      //
      if (this.rodada === 4) {
        this.encerrarJogo.emit('vitoria');
      }

      // Atualiza o objeto rodadaFrase
      this.atualizaRodada();
    } else {
      // Diminuir a variavel tentativas
      this.tentativas--;
      if (this.tentativas === -1) {
        this.encerrarJogo.emit('derrota');
      }

    }
  }

  atualizaRodada() {
    this.rodadaFrase = this.frases[this.rodada];
    console.log(this.rodadaFrase);
    // limpar a resposta do usuario
    this.resposta = '';
  }

}
