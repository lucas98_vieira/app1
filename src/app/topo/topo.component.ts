import { Component } from '@angular/core';

//Função que diz pro angular que esse arquivo é um documento
@Component({
    selector: 'app-topo',
    templateUrl: './topo.component.html',
    styleUrls:  ['./topo.component.css']
}) 

export class TopoComponent {
    public titulo: string = 'Aprendendo Inglês';

}